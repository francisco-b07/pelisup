export interface Trending {
  video: boolean;
  vote_average: number;
  title: string;
  overview: string;
  release_date: Date;
  adult: boolean;
  backdrop_path: string;
  vote_count: number;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  poster_path: string;
  popularity: number;
  media_type: string;
}
