import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, observable } from 'rxjs';
import { Trending } from 'src/interfaces/trending';
import { MoviePopular } from 'src/interfaces/movie_popular';
import { TvPopular } from 'src/interfaces/tv_popular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { MovieSerieBase, MovieSerieUser } from 'src/interfaces/movieSerie';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {
  private app_id: string = '24a0c95b6c0294e45ab93237d12c7c08';

  private baseUrl: string = 'https://api.themoviedb.org/3/';
  constructor(private _http: HttpClient, private firestore: AngularFirestore) {}

  addUser(userId: MovieSerieUser): Promise<any> {
    return this.firestore.collection('usuario').add(userId);
  }

  addItem(userId: MovieSerieUser, item: any, media_type: string): Promise<any> {
    item.media_type = media_type;
    return this.firestore
      .collection('usuarios')
      .doc(`${userId}`)
      .collection('movies')
      .add(item);
  }

  getList(userId: MovieSerieUser): Observable<any> {
    return this.firestore
      .collection('usuarios')
      .doc(`${userId}`)
      .collection('movies')
      .snapshotChanges();
  }

  deleteItem(idUser: string, id: string): Promise<any> {
    return this.firestore
      .collection(`usuarios/${idUser}/movies`)
      .doc(id)
      .delete();
  }

  getTrending(): Observable<Trending[]> {
    let params = new HttpParams().set('api_key', this.app_id);
    return this._http.get<Trending[]>(this.baseUrl + 'trending/all/week', {
      params: params,
    });
  }
  getMovies(): Observable<MoviePopular[]> {
    let params = new HttpParams().set('api_key', this.app_id);
    return this._http.get<MoviePopular[]>(this.baseUrl + 'movie/popular', {
      params: params,
    });
  }
  getSeries(): Observable<TvPopular[]> {
    let params = new HttpParams().set('api_key', this.app_id);
    return this._http.get<TvPopular[]>(this.baseUrl + 'tv/popular', {
      params: params,
    });
  }
  getPorId(media_type: string, id: number): Observable<TvPopular[]> {
    let params = new HttpParams().set('api_key', this.app_id);
    return this._http.get<any>(this.baseUrl + media_type + '/' + id, {
      params: params,
    });
  }
  public addMovieSerie(value: MovieSerieUser, type: string) {
    if (type === 'tv') return this.firestore.collection('tv').add(value);
    else return this.firestore.collection('movie').add(value);
  }
}
