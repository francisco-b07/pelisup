import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  errCode: string = '';
  constructor(public afauth: AngularFireAuth) {}

  async login(email: string, password: string) {
    this.errCode = '';
    try {
      return await this.afauth.signInWithEmailAndPassword(email, password);
    } catch (err: any) {
      console.log('error en login: ', err.code);
      this.errCode = err.code;
      return null;
    }
  }

  async loginWithGoogle() {
    try {
      return await this.afauth.signInWithPopup(
        new firebase.auth.GoogleAuthProvider()
      );
    } catch (err) {
      console.log('error en login con google: ', err);
      return null;
    }
  }

  async register(email: string, password: string) {
    try {
      return await this.afauth.createUserWithEmailAndPassword(email, password);
    } catch (err) {
      console.log('Error en register: ', err);
      return null;
    }
  }

  public getUserLogged() {
    return this.afauth.authState;
  }
  public logOut() {
    return this.afauth.signOut();
    // this.afauth
    //   .signOut()
    //   .then(() => {
    //     console.log('Sesion cerrada');
    //   })
    //   .catch((err) => {
    //     console.log('Error: ', err);
    //   });
  }
}
