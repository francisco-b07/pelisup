import { Injectable } from '@angular/core';
// import { Component } from '@angular/core';
// import { User } from '../../../interfaces/user';
import { AuthService } from '../Auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  user: any;
  userJSON: string | null = null;

  constructor(private authService: AuthService) {}

  setLocalStorage() {
    localStorage.setItem('Usuario', JSON.stringify(this.user));
  }

  getLocalStorage() {
    this.userJSON = localStorage.getItem('Usuario');
    if (this.userJSON) {
      this.user = JSON.parse(this.userJSON);
    }
  }

  removeLocalStorage() {
    localStorage.removeItem('Usuario');
  }
}

// Obtener catidad de elementos en localStorage.
// localStorage.length;

// Setear elemento dentro del localStorage.
// localStorage.setItem('Nombre', JSON.stringify(this.test));

// Recuperar elemento del localStorage.
// localStorage.getItem('Nombre');

// Remover un elemento del localStorage.
// localStorage.removeItem('Nombre');

// Vaciar localStorage.
// localStorage.clear();
