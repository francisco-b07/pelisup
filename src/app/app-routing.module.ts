import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgregarComponent } from './components/routes/agregar/agregar.component';
import { IngresarComponent } from './components/routes/ingresar/ingresar.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { MiListaComponent } from './components/routes/mi-lista/mi-lista.component';
import { PelisComponent } from './components/routes/pelis/pelis.component';
import { RegisterComponent } from './components/routes/register/register.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { WelcomeComponent } from './components/routes/welcome/welcome.component';

const routes: Routes = [
  {
    path: 'Ingresar',
    component: IngresarComponent,
  },
  {
    path: 'Inicio',
    component: InicioComponent,
  },
  {
    path: 'Pelis',
    component: PelisComponent,
  },
  {
    path: 'Series',
    component: SeriesComponent,
  },
  {
    path: 'Mi-Lista',
    component: MiListaComponent,
  },
  {
    path: 'Welcome',
    component: WelcomeComponent,
  },
  {
    path: 'Agregar',
    component: AgregarComponent,
  },
  {
    path: 'Register',
    component: RegisterComponent,
  },
  {
    path: '**',
    redirectTo: 'Inicio',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
