import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/Auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  public userLogged: Observable<any> = this.authService.afauth.user;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    console.log(localStorage.getItem('Usuario'));
  }

  public logOut() {
    this.authService.logOut();
    this.router.navigate(['/Inicio']);
    localStorage.removeItem('Usuario');
  }
}
