import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { TvPopular } from 'src/interfaces/tv_popular';
import { MovieSerieBase } from 'src/interfaces/movieSerie';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css'],
})
export class SeriesComponent implements OnInit {
  toSearch = '';
  series: TvPopular[] = [];
  encountered: any[] = [];
  eliminar: boolean = true;
  logueado: boolean = false;
  encountered_all: any[] = [];

  constructor(private _moviesService: MoviesService) {}

  ngOnInit(): void {
    if (localStorage.getItem('Usuario') == null) {
      this.logueado = false;
      this.getSeries();
    } else {
      this.logueado = true;
      this.getList();
    }
  }

  search() {
    this.encountered = this.series;
    this.encountered = this.series.filter((res) => {
      return res.name
        .toLocaleLowerCase()
        .match(this.toSearch.toLocaleLowerCase());
    });
  }

  getList() {
    let user = JSON.parse(localStorage.getItem('Usuario') || '');
    this._moviesService.getList(user.uid).subscribe({
      next: (data: any) => {
        for (let index = 0; index < data.length; index++) {
          const item =
            data[index].payload._delegate.doc._document.data.value.mapValue
              .fields;
          console.log(item);

          if (item.media_type.stringValue == 'tv' && item.id.integerValue) {
            item.docId =
              data[index].payload._delegate.doc._document.key.path.segments[8];
            this.series.push(item);
          }
        }

        this.encountered = this.series;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  getSeries() {
    this._moviesService.getSeries().subscribe({
      next: (data: any) => {
        this.series = data.results;
        this.encountered_all = this.series;
        this.encountered = this.series;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  count() {
    return this.encountered.length;
  }
}
