import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { PelisComponent } from './pelis/pelis.component';
import { SeriesComponent } from './series/series.component';
import { IngresarComponent } from './ingresar/ingresar.component';
import { CardComponent } from '../shared/card/card.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MiListaComponent } from './mi-lista/mi-lista.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AgregarComponent } from './agregar/agregar.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    InicioComponent,
    PelisComponent,
    SeriesComponent,
    IngresarComponent,
    MiListaComponent,
    WelcomeComponent,
    AgregarComponent,
    RegisterComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
    InicioComponent,
    PelisComponent,
    SeriesComponent,
    IngresarComponent,
  ],
})
export class RoutesModule {}
