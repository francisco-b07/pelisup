import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MoviePopular } from 'src/interfaces/movie_popular';
import { TvPopular } from 'src/interfaces/tv_popular';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css'],
})
export class WelcomeComponent implements OnInit {
  cantidad: number = 0;
  encontrared: [] = [];
  series: TvPopular[] = [];
  movies: MoviePopular[] = [];
  constructor(private _moviesService: MoviesService) {}

  ngOnInit(): void {
    this.getListPeliculas();
    this.getListSeries();
  }
  getListSeries() {
    let user = JSON.parse(localStorage.getItem('Usuario') || '');
    this._moviesService.getList(user.uid).subscribe({
      next: (data: any) => {
        for (let index = 0; index < data.length; index++) {
          const item =
            data[index].payload._delegate.doc._document.data.value.mapValue
              .fields;
          console.log(item);

          if (item.media_type.stringValue == 'tv' && item.id.integerValue) {
            item.docId =
              data[index].payload._delegate.doc._document.key.path.segments[8];
            this.series.push(item);
          }
        }
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  getListPeliculas() {
    let user = JSON.parse(localStorage.getItem('Usuario') || '');
    this._moviesService.getList(user.uid).subscribe({
      next: (data: any) => {
        for (let index = 0; index < data.length; index++) {
          const item =
            data[index].payload._delegate.doc._document.data.value.mapValue
              .fields;
          if (item.media_type.stringValue == 'movie') {
            item.docId =
              data[index].payload._delegate.doc._document.key.path.segments[8];
            this.movies.push(item);
          }
        }
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  countSeries() {
    return this.series.length;
  }
  countPeliculas() {
    return this.movies.length;
  }
}
