import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  user: any;
  userJSON: string | null = null;

  hide: boolean = true;
  invalidForm: boolean = true;
  errCode: string = '';

  miFormulario: FormGroup;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private afauth: AngularFireAuth
  ) {
    this.miFormulario = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  // private afauth: AngularFireAuth

  ngOnInit(): void {}

  myFunction() {
    this.hide = !this.hide;
  }
  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }

  guardar() {
    if (this.miFormulario.invalid) {
      this.miFormulario.markAllAsTouched();
      return;
    }
    const email = this.miFormulario.value.email;
    const password = this.miFormulario.value.password;

    this.afauth
      .createUserWithEmailAndPassword(email, password)
      .then((user: any) => {
        this.router.navigate(['/Ingresar']);
      })
      .catch((err: any) => {
        alert(this.firbaseError(err.code));
      });
    this.miFormulario.reset();
  }
  firbaseError(code: string) {
    switch (code) {
      case 'auth/email-already-in-use':
        return 'El usuario ya existe';
      case 'auth/invalid-email':
        return 'Correo invalido';
      default:
        return 'Error Desconocido';
    }
  }

  // Registrarme() {
  //   if (this.miFormulario.invalid) {
  //     this.invalidForm = true;
  //     return;
  //   }
  //   this.invalidForm = false;

  //   const { email, password } = this.miFormulario.value;
  //   this.user = { email, password };
  //   this.authService.register(email, password).then((res) => {
  //     console.log(res);

  //     console.log('Se registro: ', res);
  //     this.errCode = this.authService.errCode;
  //     console.log('el error es: ' + this.errCode);
  //     this.router.navigate(['/Ingresar']);
  //   });
  // }
  IngresarConGoogle() {
    console.log('Ingresar');

    this.authService.loginWithGoogle().then((res) => {
      if (!res) return;
      localStorage.setItem('Usuario', JSON.stringify(res.user));
      console.log('Se registro: ', res);
      this.router.navigate(['/Welcome']);
    });
  }

  setLocalStorage() {
    localStorage.setItem('Usuario', JSON.stringify(this.user));
  }
  getLocalStorage() {
    this.userJSON = localStorage.getItem('Usuario');
    if (this.userJSON) {
      this.user = JSON.parse(this.userJSON);
    }
  }

  removeLocalStorage() {
    localStorage.removeItem('Usuario');
  }
}
