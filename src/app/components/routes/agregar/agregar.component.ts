import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MoviePopular } from 'src/interfaces/movie_popular';
import { Trending } from 'src/interfaces/trending';
import { TvPopular } from 'src/interfaces/tv_popular';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css'],
  providers: [MoviesService],
})
export class AgregarComponent implements OnInit {
  buscar: boolean = false;
  trending: Trending[] = [];
  movies: MoviePopular[] = [];
  series: TvPopular[] = [];
  filter = 'Todo';
  toSearch = '';
  encountered_all: any[] = [];
  encountered: any[] = [];
  agregar: boolean = true;

  constructor(private _moviesService: MoviesService) {}

  ngOnInit(): void {
    // this.getTrending('Todo');
  }
  getTrending() {
    this._moviesService.getTrending().subscribe({
      next: (data: any) => {
        for (const result of data.results) {
          if (result.media_type == 'movie' || result.media_type == 'tv') {
            this.trending = data.results;
          }
        }
        this.encountered_all = this.trending;
        this.encountered = this.trending;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  search() {
    this.encountered = this.encountered_all.filter((res) => {
      if (res.title) {
        return res.title
          .toLocaleLowerCase()
          .match(this.toSearch.toLocaleLowerCase());
      }
      if (res.name) {
        return res.name
          .toLocaleLowerCase()
          .match(this.toSearch.toLocaleLowerCase());
      }
    });
  }
  public count() {
    return this.encountered.length;
  }
}
