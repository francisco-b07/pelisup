import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MovieSerieBase } from 'src/interfaces/movieSerie';
import { MoviePopular } from 'src/interfaces/movie_popular';

@Component({
  selector: 'app-pelis',
  templateUrl: './pelis.component.html',
  styleUrls: ['./pelis.component.css'],
  providers: [MoviesService],
})
export class PelisComponent implements OnInit {
  toSearch = '';
  movies: MoviePopular[] = [];
  eliminar: boolean = true;
  encountered: any[] = [];
  encountered_all: any[] = [];
  logueado: boolean = false;

  constructor(private _moviesService: MoviesService) {}

  ngOnInit(): void {
    if (localStorage.getItem('Usuario') == null) {
      this.logueado = false;
      this.getMovies();
    } else {
      this.logueado = true;
      this.getList();
    }
  }

  getList() {
    let user = JSON.parse(localStorage.getItem('Usuario') || '');
    this._moviesService.getList(user.uid).subscribe({
      next: (data: any) => {
        for (let index = 0; index < data.length; index++) {
          const item =
            data[index].payload._delegate.doc._document.data.value.mapValue
              .fields;
          if (item.media_type.stringValue == 'movie') {
            item.docId =
              data[index].payload._delegate.doc._document.key.path.segments[8];
            this.movies.push(item);
          }
        }

        this.encountered = this.movies;
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
  getMovies() {
    this._moviesService.getMovies().subscribe({
      next: (data: any) => {
        this.movies = data.results;
        this.encountered_all = this.movies;
        this.encountered = this.movies;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }
  search() {
    this.encountered = this.movies;
    this.encountered = this.movies.filter((res) => {
      return res.title
        .toLocaleLowerCase()
        .match(this.toSearch.toLocaleLowerCase());
    });
  }
  count() {
    return this.encountered.length;
  }
}
