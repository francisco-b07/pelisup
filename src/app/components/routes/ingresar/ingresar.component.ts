import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css'],
})
export class IngresarComponent implements OnInit {
  user: any;
  userJSON: string | null = null;
  loading: boolean = false;
  hide: boolean = true;
  invalidForm: boolean = true;
  errCode: string = '';

  miFormulario: FormGroup;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private afAuth: AngularFireAuth
  ) {
    this.miFormulario = this.fb.group({
      email: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  // private afauth: AngularFireAuth

  ngOnInit(): void {}

  myFunction() {
    this.hide = !this.hide;
  }
  campoEsValido(campo: string) {
    return (
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }

  guardar() {
    if (this.miFormulario.invalid) {
      this.miFormulario.markAllAsTouched();
      return;
    }

    const email = this.miFormulario.value.email;
    const password = this.miFormulario.value.password;
    this.loading = true;
    this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.loading = false;
        this.router.navigate(['/Welcome']);
      })
      .catch((error) => {
        this.loading = false;
      });

    this.miFormulario.reset();
  }

  Ingresar() {
    if (this.miFormulario.invalid) {
      this.invalidForm = true;
      return;
    }
    this.invalidForm = false;

    const { email, password } = this.miFormulario.value;
    this.user = { email, password };
    this.authService.login(email, password).then((res) => {
      console.log(res);

      if (!res) {
        this.authService.register(email, password).then((resp) => {
          console.log(resp);
        });
      }

      console.log('Se registro: ', res);
      this.errCode = this.authService.errCode;
      console.log('el error es: ' + this.errCode);
      this.router.navigate(['/Welcome']);
    });
  }
  IngresarConGoogle() {
    console.log('Ingresar');

    this.authService.loginWithGoogle().then((res) => {
      if (!res) return;
      localStorage.setItem('Usuario', JSON.stringify(res.user));
      console.log('Se registro: ', res);
      this.router.navigate(['/Welcome']);
    });
  }

  setLocalStorage() {
    localStorage.setItem('Usuario', JSON.stringify(this.user));
  }
  getLocalStorage() {
    this.userJSON = localStorage.getItem('Usuario');
    if (this.userJSON) {
      this.user = JSON.parse(this.userJSON);
    }
  }

  removeLocalStorage() {
    localStorage.removeItem('Usuario');
  }
}
