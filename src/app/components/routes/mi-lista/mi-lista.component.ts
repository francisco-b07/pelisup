import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MovieSerie } from 'src/interfaces/movieSerie';

@Component({
  selector: 'app-mi-lista',
  templateUrl: './mi-lista.component.html',
  styleUrls: ['./mi-lista.component.css'],
})
export class MiListaComponent implements OnInit {
  public filter: string = 'Todos';
  user: any;
  movies: MovieSerie[] = [];

  constructor(private _movieService: MoviesService) {}

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('Usuario') || '');
    this.getMovies();
  }

  getMovies() {
    this._movieService.getList(this.user.uid).subscribe(
      (res) => {
        this.movies = [];
        res.forEach((element: any) => {
          console.log(element.payload.doc.id);
          console.log(element.payload.doc.data());
          this.movies.push({
            idGlobal: element.payload.doc.id,
            ...element.payload.doc.data(),
          });
        });
        console.log('esta es la peticion de movies', this.movies);
      },
      (error) => {
        console.log('falló la petición de movies', error);
      }
    );
  }
  deleteItem(id: string) {
    this._movieService
      .deleteItem(this.user.uid, id)
      .then(() => {
        console.log('se eliminó correctamente');
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
