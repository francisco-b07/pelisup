import { Component, OnInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MovieSerieBase } from 'src/interfaces/movieSerie';
import { MoviePopular } from 'src/interfaces/movie_popular';
import { Trending } from 'src/interfaces/trending';
import { TvPopular } from 'src/interfaces/tv_popular';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [MoviesService],
})
export class InicioComponent implements OnInit {
  trending: Trending[] = [];
  movies: MoviePopular[] = [];
  series: TvPopular[] = [];
  filter = 'Todo';
  toSearch = '';
  encountered_all: any[] = [];
  encountered: any[] = [];
  agregar: boolean = false;

  constructor(private _moviesService: MoviesService) {}

  ngOnInit(): void {
    this.getTrending('Todo');
  }
  getTrending(seleccionado: string) {
    this.filter = seleccionado;
    this._moviesService.getTrending().subscribe({
      next: (data: any) => {
        for (const result of data.results) {
          if (result.media_type == 'movie' || result.media_type == 'tv') {
            this.trending = data.results;
          }
        }
        this.encountered_all = this.trending;
        this.encountered = this.trending;
        console.log(this.encountered);
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  getMovies(seleccionado: string) {
    this.filter = seleccionado;
    this._moviesService.getMovies().subscribe({
      next: (data: any) => {
        this.movies = data.results;
        this.encountered_all = this.movies;
        this.encountered = this.movies;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  getSeries(seleccionado: string) {
    this.filter = seleccionado;
    this._moviesService.getSeries().subscribe({
      next: (data: any) => {
        this.series = data.results;
        this.encountered_all = this.series;
        this.encountered = this.series;
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }

  search() {
    this.encountered = this.encountered_all.filter((res) => {
      if (res.title) {
        return res.title
          .toLocaleLowerCase()
          .match(this.toSearch.toLocaleLowerCase());
      }
      if (res.name) {
        return res.name
          .toLocaleLowerCase()
          .match(this.toSearch.toLocaleLowerCase());
      }
    });
  }
  public count() {
    return this.encountered.length;
  }
}
