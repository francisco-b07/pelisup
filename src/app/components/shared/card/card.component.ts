import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/Auth/auth.service';
import { MoviesService } from 'src/app/services/movies/movies.service';
import { MovieSerieBase } from 'src/interfaces/movieSerie';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent implements OnInit {
  public userLogged: Observable<any> = this.authService.afauth.user;

  @Input() titulo: string = '';
  @Input() imagen: string = '';
  @Input() rating: number = 0;
  @Input() media_type: string = '';
  @Input() id: number = 0;
  @Input() agregar: boolean = false;
  @Input() eliminar: boolean = false;
  @Input() docId: string = '';
  item: any;
  constructor(
    private authService: AuthService,
    private _moviesService: MoviesService
  ) {}

  ngOnInit(): void {}
  public addList() {
    let user = JSON.parse(localStorage.getItem('Usuario') || '');
    this._moviesService.getPorId(this.media_type, this.id).subscribe({
      next: (data: any) => {
        console.log(data);
        this._moviesService

          .addItem(user.uid, data, this.media_type)
          .then(() => {
            console.log('se agregó el item al usuario');
          })
          .catch((error) => {
            console.log(error);
          });
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('complete');
      },
    });
  }
  deleteItem() {
    let user = JSON.parse(localStorage.getItem('Usuario') || '');
    this._moviesService
      .deleteItem(user.uid, this.docId)
      .then(() => {
        console.log('se elimino el item al usuario');
        setTimeout(() => {
          window.location.reload();
        }, 100);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  cambiarEstado() {
    this.agregar = !this.agregar;
    this.eliminar = !this.eliminar;
  }
}
