import { initializeApp } from 'firebase/app';
import { getAnalytics } from 'firebase/analytics';

export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyCBc3i5SrwnL79zAKtljJwFp25ku7zttgU',
    authDomain: 'curso-folcademy-front-end.firebaseapp.com',
    // databaseURL: '',
    projectId: 'curso-folcademy-front-end',
    storageBucket: 'curso-folcademy-front-end.appspot.com',
    messagingSenderId: '416972985436',
    appId: '1:416972985436:web:29773d5831ceb7f5f3c299',
    measurementId: 'G-8TEKJCP3PH',
  },
};

// Initialize Firebase
const app = initializeApp(environment.firebaseConfig);
const analytics = getAnalytics(app);
