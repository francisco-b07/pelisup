// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// Import the functions you need from the SDKs you need
// import { initializeApp } from 'firebase/app';
// import { getAnalytics } from 'firebase/analytics';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCBc3i5SrwnL79zAKtljJwFp25ku7zttgU',
    authDomain: 'curso-folcademy-front-end.firebaseapp.com',
    // databaseURL: '',
    projectId: 'curso-folcademy-front-end',
    storageBucket: 'curso-folcademy-front-end.appspot.com',
    messagingSenderId: '416972985436',
    appId: '1:416972985436:web:29773d5831ceb7f5f3c299',
    measurementId: 'G-8TEKJCP3PH',
  },
};

// Initialize Firebase
// const app = initializeApp(environment.firebaseConfig);
// const analytics = getAnalytics(app);
